function composeAlert(result, msg, msgDetail) {
    console.log(result + ' / ' + msg + ' / ' + msgDetail);
    let composedAlertTag = `
            <div class="alert alert-${result} alert-dismissible fade show" role="alert">
                <strong>${msg}</strong> ${msgDetail}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        `
    console.log(composedAlertTag);
    return composedAlertTag;
}

function makeTable(json, divId, tableName, nameElementId) {
    console.log(json);
    $('#' + divId).empty();

    let row = '<tbody>';
    $.each(json, function (idx, e) {
        row = row + '<tr>';

        if (idx === 0) {
            let key = Object.keys(e);
            let header = '<thead><tr>'
            $.each(key, function (j, val) {
                header = header + '<th>' + val + '</th>';
            })
            header = header + '</tr></thead>'
            $('#' + divId).append(header);
        }

        $.each(e, function (i, value) {
            row = row + '<td>' + value + '</td>';
        })
        row = row + '</tr>'
        // $('#'+divId).append(row);
    });
    row = row + '</tbody>';
    // $('#dataTable').DataTable();

    $('#' + divId).append(row);
    $('#' + divId).DataTable({
        destroy: true
    });

    $('#' + nameElementId).text(tableName + ' 테이블 메타 정보');
}


function select(selectTag, saveInputElement) {
    strText = null;
    strText = $(selectTag).text();
    console.log(strText);

    $("#" + saveInputElement).empty();
    $("#" + saveInputElement).val(strText);

    $.ajax({
        url: 'api/col/list/' + strText,
        method: 'GET',
        dataType: 'json'
    })
        .done(function (json) {
            makeTable(json, 'dataTable', strText, 'displayTableName');
        })
}

