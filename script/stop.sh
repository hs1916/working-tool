#!/bin/bash
IMD_PORTAL_PORT=8080

pid="$(lsof -t -i :$IMD_PORTAL_PORT -s TCP:LISTEN)";

if [ "$pid" != "" ]; then
    kill -9 $pid;
    echo "$IMD_PORTAL_PORT port num";
    echo "$pid process kill complete";
else
    echo "port num $IMD_PORTAL_PORT";
    echo "pid is empty";
fi