#!/bin/bash
export STAGE=$(echo $DEPLOYMENT_GROUP_NAME | awk -F'-' '{print $1}')

HEALTH_CHECK_URL=http://127.0.0.1:8080/management/health
HEALTH_CHECK_PATTERN='"status":"UP"'


echo "> Health check 시작"
echo "> curl -s $HEALTH_CHECK_URL "

for RETRY_COUNT in {1..100}
do
  RESPONSE=$(curl -s $HEALTH_CHECK_URL)
  UP_COUNT=$(echo $RESPONSE | grep "${HEALTH_CHECK_PATTERN}" | wc -l)

  if [ $UP_COUNT -ge 1 ]
  then
    echo "> Health check 성공"
    exit 0
  else
    echo "> Health check의 응답을 알 수 없거나 혹은 status가 UP이 아닙니다."
    echo "> Health check: ${RESPONSE}"
  fi

  ERROR_COUNT=$(tail -n 1000 /logs001/tomcat/catalina.out | grep "Context \[\] startup failed due to previous errors" | wc -l)
  if [ $ERROR_COUNT -ge 1 ]
  then
    echo "> 서비스 기동에 실패 하였습니다."
    break
  fi

  echo "> Health check 연결 실패. 재시도..."
  sleep 5
done

echo "> Health check 실패. "
tail -n 500 /logs001/imd-portal/log.txt
exit 1
