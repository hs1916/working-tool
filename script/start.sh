#!/bin/bash
PORTAL_PORT=8080

pid="$(lsof -t -i :$PORTAL_PORT -s TCP:LISTEN)";

if [ "$pid" != "" ]; then
    kill -9 $pid;
    echo "$PORTAL_PORT port num";
    echo "$pid process kill complete";
else
    echo "port num $PORTAL_PORT";
    echo "pid is empty";
fi

cd /sorc001/working-tool
source profile.env
cat profile.env
echo SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}


nohup java -Dspring.profiles.active=${SPRING_PROFILES_ACTIVE} \
-Dlogback.file.path=/logs001/working-tool \
-Dlinker.file.path=/data001/working-tool/files \
-Dspring.datasource.hikari.primary.auto-commit=false \
-Dspring.datasource.hikari.secondary.auto-commit=true \
-Xms512m -Xmx1024m -jar /sorc001/working-tool/workingtool-0.0.1-SNAPSHOT.jar > /logs001/working-tool/log.txt 2>&1 &
